import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css';

import Users from './components/Users';

function App() {
  return (
    <Router>
      <Route path='/users' component={Users} />
    </Router>
  );
}

export default App;
