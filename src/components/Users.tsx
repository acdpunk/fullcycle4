import React, { Component } from 'react';
import axios from 'axios';

type User = {
  id: number,
  first_name: string,
  last_name: string,
  email: string,
  avatar: string
}

type UsersState = {
  data: User[]
}

export default class Users extends Component<{}, UsersState> {

  apiUrl = 'https://reqres.in/api/users'

  componentWillMount() {
    this.setState({
      data: []
    });
  }

  componentDidMount() {
    const _self = this;
    axios.get(this.apiUrl)
      .then(function (response) {
        if(response && response.status === 200 && response.data.data) {
          _self.setState({
            data: response.data.data
          });
        }
      });
  }

  render() {
    return (
      <div>
        <table className="table table-borderless">
        <tbody>
          {this.state.data.map((user) => (
            <tr>
              <td className="avatar-width">
                <img src={user.avatar} alt={user.first_name} className="img-thumbnail"/>
              </td>
              <td>
                <h5 className="card-title mt-2"># {user.id}</h5>
                <h5 className="card-title">{user.first_name} {user.last_name}</h5>
                <h6 className="card-subtitle mt-2 text-muted">{user.email}</h6>
                <h6 className="card-subtitle mt-2 text-muted">{user.avatar}</h6>
              </td>
            </tr>
          ))}
          </tbody>
        </table>
      </div>
    )
  }
}
